# OpenML dataset: French-Motor-Claims-Datasets-freMTPL2freq

https://www.openml.org/d/43593

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
In the dataset freMTPL2freq risk features and claim numbers were collected for 677,991 motor third-part liability policies (observed on a year).
Content
freMTPL2freq contains 11 columns (+IDpol):  IDpol The policy ID (used to link with the claims dataset).  ClaimNb Number of claims during the exposure period.  Exposure The exposure period.  Area The area code.  VehPower The power of the car (ordered categorical).  VehAge The vehicle age, in years.  DrivAge The driver age, in years (in France, people can drive a car at 18).  BonusMalus Bonus/malus, between 50 and 350: 100 means malus in France.  VehBrand The car brand (unknown categories).  VehGas The car gas, Diesel or regular.  Density The density of inhabitants (number of inhabitants per km2) in the city the driver of the car lives in.  Region The policy regions in France (based on a standard French classification)
Acknowledgements
Source: 
R-Package CASDatasets, Version 1.0-6 (2016) by Christophe Dutang [aut, cre], Arthur Charpentier [ctb]
Inspiration
The Swiss Actuarial Society's data science tutorials ( https://www.actuarialdatascience.org/ADS-Tutorials/ ) are build on the original dataset (see above) .  This copy enables the use of notebooks (kernels) to further study this interesting topic.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43593) of an [OpenML dataset](https://www.openml.org/d/43593). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43593/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43593/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43593/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

